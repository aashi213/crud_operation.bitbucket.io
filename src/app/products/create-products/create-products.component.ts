import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-products',
  templateUrl: './create-products.component.html',
  styleUrls: ['./create-products.component.scss']
})
export class CreateProductsComponent implements OnInit {
  // createForm:FormGroup;

  constructor( private productsService:ProductsService) {
   
   }

  ngOnInit(): void {
    // this.initializeLoginForm();
  }
//   initializeLoginForm(){
//     this.createForm = this._fb.group({
//       product_name: ['', [Validators.required, Validators.email]],
//       product_category: ['', Validators.required],
//       product_description:['',Validators.required],
//       product_rating:['',Validators.required],
//       product_color:['',Validators.required],
//       product_available:['',Validators.required],
//       product_price:['',Validators.required],
//       product_reviews:['',Validators.required]
//     })
//   }

//     submitLogin(){
//       this.productsService.createProduct(this.getProduct).subscribe(data=>{
//         console.log(data);
//       })
//     }

//     getProduct(createForm){
// let getProduct={
//   id:10,
//   category_id:this.createForm.value.product_category,
//   description:this.createForm.value.product_description,
//   rating:this.createForm.value.product_rating,
//   price:this.createForm.value.product_price,
//   is_available:this.createForm.value.product_available,
//   color:this.createForm.value.product_color,
//   reviews:this.createForm.value.product_reviews,
//   name: this.createForm.value.product_name
// }
//     }
  
  addNewProduct(form){
    console.log(form.value);
    let newProduct={
      id:null,
      category_id:form.value.product_category,
      name:form.value.product_name,
      description:form.value.product_description,
      rating:form.value.product_rating,
      price:form.value.product_price,
      product_img:form.value.product_img,
      is_available:form.value.is_available,
      color:form.value.product_color,
      reviews:form.value.product_reviews,
    };
    console.log(newProduct);
    this.productsService.createProduct(newProduct).subscribe(data=>{
      console.log(data);

    })
  }
}
// function readURL(input) {
  
// document.getElementById("uploadimg").style.display="block";
// if(input.files && input.files[0]){
//   var reader=new FileReader();
//   reader.onload=function(e)
// {
//   document.getElementById('uploadimg').src=e.target.result;
// }
// reader.readAsDataURL(input.files[0]);
// }
// }
