export interface Product {
    id:number;//key-value pairs
    category_id:number;
    description:string;
    rating:string;
    price:number;
    product_img:string;
    is_available:boolean;
    color:string;
    warranty:number;
    name:string;
    reviews:number;
}
//this is a kind of data type that we use in our website