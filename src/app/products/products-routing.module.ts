import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './products.component';
import { CreateProductsComponent } from './create-products/create-products.component';
import { ViewProductsComponent } from './view-products/view-products.component';
import { ViewAllProductsComponent } from './view-all-products/view-all-products.component';
import { ViewAllProductsByCategoryComponent } from './view-all-products-by-category/view-all-products-by-category.component';
import { ViewAllProductsByDateComponent } from './view-all-products-by-date/view-all-products-by-date.component';
import { DeleteProductsComponent } from './delete-products/delete-products.component';
import { UpdateProductsComponent } from './update-products/update-products.component';

const routes: Routes = [
  { path: '', component: ViewAllProductsComponent },
  { path:'create-product',component:CreateProductsComponent},
  { path:'view-product/:id',component:ViewProductsComponent},//by params
  
  {path:'category/:id',component:ViewAllProductsByCategoryComponent},//by query params
  {path:'search-date',component:ViewAllProductsByDateComponent},//by query params
  {path:'delete-product/:id',component:DeleteProductsComponent},//by params
  {path:'update-product/:id',component:UpdateProductsComponent}//by params

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
