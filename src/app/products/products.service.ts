import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Product}from './product';//import Product class from product.ts
import {Observable} from 'rxjs';
import{Category} from '../site-framework/category';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpClient:HttpClient)//make instance of HttpClient
   { }
   getAllProducts(): Observable<Product>{
     const producturl='http://localhost:3000/products';
     return this.httpClient.get<Product>(producturl);
   }
   createProduct(productBody):Observable<Product>{
   const producturl='http://localhost:3000/products';
   return this.httpClient.post<Product>(producturl,productBody)//return an observable
   }
   viewProduct(productId):Observable <Product>{
    const producturl='http://localhost:3000/products/' +productId;
    return this.httpClient.get<Product>(producturl)//return an observable
    }
  updateProduct(productId,productBody):Observable<Product>{
      const producturl='http://localhost:3000/products/'+productId;
      return this.httpClient.put<Product>(producturl,productBody)//return an observable
      }
    deleteProduct(productId):Observable <Product>{
        const producturl='http://localhost:3000/products/'+productId;
        return this.httpClient.delete<Product>(producturl)//return an observable
        }
        searchCategoryProduct(categoryId):Observable <Product>{
          const producturl='http://localhost:3000/products?category_id='+categoryId;
          return this.httpClient.get<Product>(producturl)//return an observable
          }
          searchDateProduct(DateParam):Observable<Product>{
            const producturl='http://localhost:3000/products/date='+DateParam;
            return this.httpClient.get<Product>(producturl)//return an observable
            }
            getCategories():Observable<Category>{
              const categoryUrl='http://localhost:3000/categories';
              return this.httpClient.get<Category>(categoryUrl);
            }
}
