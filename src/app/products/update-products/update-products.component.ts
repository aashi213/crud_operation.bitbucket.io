import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../products.service';
import { Product } from '../product';

@Component({
  selector: 'app-update-products',
  templateUrl: './update-products.component.html',
  styleUrls: ['./update-products.component.scss']
})
export class UpdateProductsComponent implements OnInit {
  productId=0;
  productDetails:Product;
  constructor(private activatedRoute:ActivatedRoute, private productsService:ProductsService ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(data=>{
      this.productId=data.id;
      console.log(data);
      this.productsService.viewProduct(this.productId).subscribe(productData=>{
        this.productDetails=productData;//get the existing data of the product
        console.log(this.productDetails);
      })
    });
  }
  updateProduct(form){

    console.log(form);

    this.productsService.updateProduct(this.productId,form.value).subscribe(data=>{
      console.log(data);

    })
  }

}
