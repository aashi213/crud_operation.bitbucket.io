import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { Product } from '../product';
import { Category } from 'src/app/site-framework/category';

@Component({
  selector: 'app-view-all-products-by-category',
  templateUrl: './view-all-products-by-category.component.html',
  styleUrls: ['./view-all-products-by-category.component.scss']
})
export class ViewAllProductsByCategoryComponent implements OnInit {
  searchCategory: Category;
  productList: Product;
  constructor(private activatedRoute: ActivatedRoute,private productService:ProductsService,private router:Router) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(data=>{
      this.searchCategory=data.id;
      console.log(this.searchCategory);
      this.productService.searchCategoryProduct(this.searchCategory).subscribe(categorydata=>{
        console.log(categorydata);
        this.productList=categorydata;
      });
    });
  }
viewProduct(prodId){
console.log(prodId);
this.router.navigateByUrl(`products/view-product/${prodId}`)
}
}
